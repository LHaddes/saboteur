﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileManager : MonoBehaviour
{
    public GameObject gears;

    public Tilemap test;
    
    public Trail[] cards;

    public Mine[] mines = new Mine[3];

    public Tile mineTile;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");

        //setup le terrain de base
        test.SetTile(test.WorldToCell(new Vector2(-7,0)), cards[0].myTile);
        
        mines[0] = new Mine(new Vector2(Random.Range(3, 7),Random.Range(2, 5)), 0, mineTile, cards[0].myTile,true, true, true, true);
        mines[1] = new Mine(new Vector2(Random.Range(3, 7),0), 0, mineTile, cards[0].myTile,true, true, true, true);
        mines[2] = new Mine(new Vector2(Random.Range(3, 7),Random.Range(-2, -3)), 0, mineTile, cards[0].myTile,true, true, true, true);
        mines[Random.Range(0, 3)].gold = Random.Range(1, 3);
        
        test.SetTile(test.WorldToCell(mines[0].oui), mines[0].myTile);
        test.SetTile(test.WorldToCell(mines[1].oui), mines[1].myTile);
        test.SetTile(test.WorldToCell(mines[2].oui), mines[2].myTile);
    }
    
    void Update()
    {
        
    }
    
   
}

[System.Serializable]
public class Trail
{
    public bool bottomeLeft;
    public bool bottomeRight;
    
    public Tile myTile;
    
    public bool openTop;
    public bool openLeft;
    public bool openRight;
    public bool openDown;

    public Trail(Tile mytrail, bool a, bool b, bool c, bool d)
    {
        myTile = mytrail;

        openTop = a;
        openLeft = b;
        openDown = c;
        openRight = d;
    }
}

[System.Serializable]
public class Mine : Trail
{
    public int gold;

    public Vector2 oui;

    public Tile trailTile;

    public Mine(Vector2 v, int go, Tile mine, Tile trail, bool a, bool b, bool c, bool d) : base(mine, a, b, c, d)
    {
        oui = v;
        gold = go;
        trailTile = trail;
    }
}
