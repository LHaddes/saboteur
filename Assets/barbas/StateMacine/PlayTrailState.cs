﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class PlayTrailState : State
{
    public GameObject gears;
    
    public Vector2 mouseOnTheWorld;

    public Tilemap test; //tile map principale

    public Trail currentTrail;
    public Trail[] cardNextTo = new Trail[4];
    public bool[] connectedWithOne = new bool[4];
    public Vector2[] cardNextToPos = new Vector2[4];

    public PlayTrailState(TurnManager turnManager, Trail myTrail) : base(turnManager)
    {
        currentTrail = myTrail;
    }
    
    public override void OnStateEnter()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        test = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().test;
    }
    
    public override void Tick()
    {
        mouseOnTheWorld = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(Input.mousePosition);

        cardNextToPos[0] = mouseOnTheWorld + new Vector2Int(2, 0);
        cardNextToPos[1] = mouseOnTheWorld + new Vector2Int(-2, 0);
        cardNextToPos[2] = mouseOnTheWorld + new Vector2Int(0, 1);
        cardNextToPos[3] = mouseOnTheWorld + new Vector2Int(0, -1);

        if (Input.GetButtonDown("Fire1") && CanPlaceCard())
        {
            test.SetTile(test.WorldToCell(mouseOnTheWorld), currentTrail.myTile);

            foreach (var mine in gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().mines)
            {
                MineCheck(0, mine);
                MineCheck(1, mine);
                MineCheck(2, mine);
                MineCheck(3, mine);
            }

            gears.GetComponent<Gears>().sign.GetComponent<Image>().sprite = null;
            
            turnManager.SetActionState(null);
        }
        else
        {
            //print("can't place card");
        }
    }

    public bool CanPlaceCard()
    {
        if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) != null) //si il y a déja une carte sous la souris
        {
            return false;
        }
        
        bool[] evryTrailWork = new bool[4];

        bool connected = false;

        for (int i = 0; i < connectedWithOne.Length; i++) //reset le tableau
        {
            connectedWithOne[i] = false;
        }

        if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) == null &&
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(-2, 0))) == null && 
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, 1))) == null && 
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, -1))) == null)
        {
            return false;
        }
        else
        {
            foreach (var card in gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards) //relier les tiles autour de la position pointer en fonction de leur tile(les asscocier a une instance d'une class d'une list qui contient toutes les cartes)
            {
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) == card.myTile) //le switch marche po
                {
                    cardNextTo[0] = card;
                } 
                
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(-2, 0))) == card.myTile)
                {
                    cardNextTo[1] = card;
                }
                
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, 1))) == card.myTile)
                {
                    cardNextTo[2] = card;
                }
                
                if ( test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, -1))) == card.myTile)
                {
                    cardNextTo[3] = card;
                }
            }

            /*void CardNextToWork(Vector2Int v, int i, bool a, bool b) //march pa
            {
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + v)) != null)
                {
                    if (a)
                    {
                        if (b)
                        {
                            connectedWithOne[i] = true;
                            evryTrailWork[i] = true;
                        }
                    }
                    else
                    {
                        if (!b)
                        {
                            evryTrailWork[i] = true;
                        }
                    } 
                }
                else
                {
                    evryTrailWork[i] = true; 
                }
            }*/

            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) != null)//faire une fonction
            {
                 if (currentTrail.openRight)
                 {
                     if (cardNextTo[0].openLeft)
                     {
                         connectedWithOne[0] = true;
                         evryTrailWork[0] = true;
                     }
                 }
                 else
                 {
                     if (!cardNextTo[0].openLeft)
                     {
                         evryTrailWork[0] = true;
                     }
                 } 
            }
            else
            {
                 evryTrailWork[0] = true; 
            }
            
            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(-2, 0))) != null)
            {
                if (currentTrail.openLeft)
                {
                    if (cardNextTo[1].openRight)
                    {
                        connectedWithOne[1] = true;
                        evryTrailWork[1] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[1].openRight)
                    {
                        evryTrailWork[1] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[1] = true;
            }
            
            //CardNextToWork(new Vector2Int(-2, 0), 1, currentTrail.openLeft, cardNextTo[0].openRight);

            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(0, 1))) != null)
            {
                if (currentTrail.openTop)
                {
                    if (cardNextTo[2].openDown)
                    {
                        connectedWithOne[2] = true;
                        evryTrailWork[2] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[2].openDown)
                    {
                        evryTrailWork[2] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[2] = true;
            }
            

            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(0, -1))) != null)
            {
                if (currentTrail.openDown)
                {
                    if (cardNextTo[3].openTop)
                    {
                        connectedWithOne[3] = true;
                        evryTrailWork[3] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[3].openTop)
                    {
                        evryTrailWork[3] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[3] = true;
            }

            int i = 0;
            
            foreach (var co in connectedWithOne)
            {
                if (co)
                {
                    i++;
                    connected = co;
                }
            }

            if (i == 1) //check si ont essaye de mettre un chemoin a une mine toute seule et si la mine est lié a un chemin
            {
                foreach (var mine in gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().mines)
                {
                    if (test.WorldToCell(mine.oui) == test.WorldToCell(cardNextToPos[0]) && connectedWithOne[0])
                    {
                        Debug.Log(test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(2, 0))));
                        if (test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(-2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, 1))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, -1))) == null)
                        {
                            return false;
                        }
                    }
                
                    if (test.WorldToCell(mine.oui) == test.WorldToCell(cardNextToPos[1]) && connectedWithOne[1])
                    {
                        if (test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(-2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, 1))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, -1))) == null)
                        {
                            return false;
                        }
                    }
                
                    if (test.WorldToCell(mine.oui) == test.WorldToCell(cardNextToPos[2]) && connectedWithOne[2])
                    {
                        if (test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(-2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, 1))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, -1))) == null)
                        {
                            return false;
                        }
                    }
                
                    if (test.WorldToCell(mine.oui) == test.WorldToCell(cardNextToPos[3]) && connectedWithOne[3])
                    {
                        if (test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(-2, 0))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, 1))) == null && 
                            test.GetTile(test.WorldToCell(mine.oui) + test.WorldToCell(new Vector2(0, -1))) == null)
                        {
                            return false;
                        }
                    }
                }
            }
            
            if (evryTrailWork[0] && evryTrailWork[1] && evryTrailWork[2] && evryTrailWork[3] && connected)
            {
                return true;
            }
        }

        return false;
    }

    public void TurnCard(Trail t) //TODO
    {
        if (t.bottomeLeft)
        {
            t = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[1];
        }else if (t.bottomeRight)
        {
            t = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[2];
        }

        t.bottomeLeft = !t.bottomeLeft;
        t.bottomeRight = !t.bottomeRight;
    }

    public void MineCheck(int i, Mine m)
    {
        if (test.WorldToCell(m.oui) == test.WorldToCell(cardNextToPos[i]) && connectedWithOne[i])
        {
            test.SetTile(test.WorldToCell(m.oui), m.trailTile);
            Debug.Log("gold :" + m.gold);
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.goldPicked += m.gold;
            
            if (m.gold != 0)
            {
                gears.GetComponent<Gears>().Reload();
            }
        }
    }
}
