﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TurnState : State
{
    public Player currentPlayer;
    public Pioche pioche = GameObject.FindObjectOfType<Pioche>();


    public TurnState(TurnManager turnManager, Player curPlay) : base(turnManager)
    {
        currentPlayer = curPlay;
    }

    
    public override void OnStateEnter()
    {
        Debug.Log("refaire la main");
        Debug.Log(currentPlayer.name);
        turnManager.currentPlayer = currentPlayer;
        
        currentPlayer.main.SetActive(true);
        pioche.player = currentPlayer;
        pioche.SlotRemove();
        pioche.SlotUpdate();
    }

    public override void Tick()
    {
        
    }

    public override void OnStateExit()
    {
        currentPlayer.main.SetActive(false);
        currentPlayer.played = false;
    }
}
