﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Experimental.TerrainAPI;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TurnManager : MonoBehaviour
{
    [SerializeField]
    public State currentState; //les tour
    public State currentAction; //les action pendant les tours

    public GameObject gears;

    public int nbrOfPlayers;
    public int nextPlayerIndex;

    public List<Player> players = new List<Player>(4);

    public Player currentPlayer;

    private void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        //players = new List<Player>(nbrOfPlayers);
        NextPlayerTurn();
    }

    private void Update()
    {
        currentState.Tick();
        
        if (currentAction != null)
        {
            currentAction.Tick();
        }
    }

    public void SetState(State state)
    {
        if (currentState != null)
            currentState.OnStateExit();

        currentState = state;
        gameObject.name = "TurnManager - " + state.GetType().Name;

        if (currentState != null)
            currentState.OnStateEnter();
    }

    public void SetActionState(State action)
    {
        if (currentAction != null)
            currentAction.OnStateExit();

        currentAction = action;

        if (currentAction != null)
            currentAction.OnStateEnter();
    }

    public void NextPlayerTurn()
    {
        if (nextPlayerIndex == nbrOfPlayers)
        {
            nextPlayerIndex = 0;
        }
        
        SetState(new TurnState(this, players[nextPlayerIndex]));
        if (currentPlayer.saboteur)
        {
            gears.GetComponent<Gears>().uiRole.GetComponent<TextMeshProUGUI>().text = (currentPlayer + "You are : saboteur");
        }
        else
        {
            gears.GetComponent<Gears>().uiRole.GetComponent<TextMeshProUGUI>().text = (currentPlayer.name + " You are : miner");
        }
        nextPlayerIndex++;
    }

    public void PlayACard()
    {
        SetActionState(new PlayTrailState(this, gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[0]));
        currentPlayer.played = true;
    }

    public void PlayATrailCrad(Trail t)
    {
        SetActionState(new PlayTrailState(this, t));
    }
}

[System.Serializable]
public class Player
{
    public string name;
    public bool saboteur;
    public bool canPlay, played;

    [HideInInspector] public bool eboulement, isDefaussing;

    public bool pioche = true;
    public bool lampe = true;
    public bool waggon = true;

    public GameObject main;

    public int goldPicked;
    
    public void Playerr(string na)
    {
        name = na;
    }
}
