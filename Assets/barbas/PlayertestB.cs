﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayertestB : MonoBehaviour
{
    public GameObject gears;
    
    public Vector2 mouseOnTheWorld;
    
    public Tilemap test;
    public Tile tile;

    public Trail currentTrail;
    public Trail[] cardNextTo = new Trail[4];
    public bool[] connectedWithOne = new bool[4];

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        test = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().test;
    }
    
     void Update()
    {
        mouseOnTheWorld = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(Input.mousePosition);

        /*if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) == tile)
        {
            print("same");
        }
        else
        {
            print(test.WorldToCell(mouseOnTheWorld));
        }*/

        if (Input.GetButtonDown("Fire1") && CanPlaceCard())
        {
            print("Place Card");
            test.SetTile(test.WorldToCell(mouseOnTheWorld), currentTrail.myTile);
            currentTrail = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[Random.Range(0, 4)];
        }
        else
        {
            //print("can't place card");
        }
    }
    
    public bool CanPlaceCard()
    {
        if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) != null) //si il y a déja une carte sous la souris
        {
            return false;
        }
        
        bool[] evryTrailWork = new bool[4];

        for (int i = 0; i < connectedWithOne.Length; i++) //reset le tableau
        {
            connectedWithOne[i] = false;
        }

        if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) == null &&
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(-2, 0))) == null && 
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, 1))) == null && 
            test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, -1))) == null)
        {
            return false;
        }
        else
        {
            foreach (var card in gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards) //relier les tiles autour de la position pointer en fonction de leur tile(les asscocier a une instance d'une class d'une list qui contient toutes les cartes)
            {
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) == card.myTile) //le switch marche po
                {
                    cardNextTo[0] = card;
                } 
                
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(-2, 0))) == card.myTile)
                {
                    cardNextTo[1] = card;
                }
                
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, 1))) == card.myTile)
                {
                    cardNextTo[2] = card;
                }
                
                if ( test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, -1))) == card.myTile)
                {
                    cardNextTo[3] = card;
                }
            }
            
            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2Int(2, 0))) != null) //faire une fonction
            {
                if (currentTrail.openRight)
                {
                    if (cardNextTo[0].openLeft)
                    {
                        connectedWithOne[0] = true;
                        evryTrailWork[0] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[0].openLeft)
                    {
                        evryTrailWork[0] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[0] = true;
            }

            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(-2, 0))) != null)
            {
                if (currentTrail.openLeft)
                {
                    if (cardNextTo[1].openRight)
                    {
                        connectedWithOne[1] = true;
                        evryTrailWork[1] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[1].openRight)
                    {
                        evryTrailWork[1] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[1] = true;
            }
            
            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, 1))) != null)
            {
                if (currentTrail.openTop)
                {
                    if (cardNextTo[2].openDown)
                    {
                        connectedWithOne[2] = true;
                        evryTrailWork[2] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[2].openDown)
                    {
                        evryTrailWork[2] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[2] = true;
            }

            if (test.GetTile(test.WorldToCell(mouseOnTheWorld + new Vector2(0, -1))) != null)
            {
                if (currentTrail.openDown)
                {
                    if (cardNextTo[3].openTop)
                    {
                        connectedWithOne[3] = true;
                        evryTrailWork[3] = true;
                    }
                }
                else
                {
                    if (!cardNextTo[3].openTop)
                    {
                        evryTrailWork[3] = true;
                    }
                }
            }
            else
            {
                evryTrailWork[3] = true;
            }
            
            if (evryTrailWork[0] && evryTrailWork[1] && evryTrailWork[2] && evryTrailWork[3])
            {
                return true;
            }
        }

        return false;
    }

    public void TurnCard(Trail t)
    {
        if (t.bottomeLeft)
        {
            t = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[1];
        }else if (t.bottomeRight)
        {
            t = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().cards[2];
        }

        t.bottomeLeft = !t.bottomeLeft;
        t.bottomeRight = !t.bottomeRight;
    }
}
