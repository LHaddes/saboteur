﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrailCardDisplay : MonoBehaviour
{
    public GameObject gears;
    
    public CarteChemin display;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        
        if (GetComponent<Image>() != null)
        {
            GetComponent<Image>().sprite = display.mySprite;
        }
        
        display.gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    void Update()
    {
        if (display.follow)
        {
            gears.GetComponent<Gears>().sign.GetComponent<Image>().sprite = display.mySprite;
        }
    }
}
