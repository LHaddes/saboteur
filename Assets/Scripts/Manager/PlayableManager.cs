﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableManager : MonoBehaviour
{
    public Gears gears;
    public TurnManager turnManager;
    
    public GameObject parentButtonsCasses, parentButtonsRepares;
    public List<GameObject> buttonsCasses;
    public List<GameObject> buttonsRepares;

    public string objetCasses, objetRepares;

    void Start()
    {
        gears = FindObjectOfType<Gears>();
        turnManager = FindObjectOfType<TurnManager>();
    }

    void Update()
    {
        for (int i = 0; i < turnManager.players.Count; i++)
        {
            if (turnManager.players[i].pioche && turnManager.players[i].waggon && turnManager.players[i].lampe)
            {
                turnManager.players[i].canPlay = true;
            }
            else
            {
                turnManager.players[i].canPlay = false;
            }
        }
    }

    public void MalusPlay(string objet)
    {
        objetCasses = objet;
        
        
        foreach (Transform child in parentButtonsCasses.transform)
        {
            buttonsCasses.Add(child.gameObject);
        }
        
        for (int i = 0; i < buttonsCasses.Count; i++)
        {
            buttonsCasses[i].SetActive(true);
            if (buttonsCasses[i].name == gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>()
                    .currentPlayer.name)
            {
                buttonsCasses[i].SetActive(false);
            }
        }
    }

    #region MalusForAllPlayers
    public void MalusJoueur1()
    {
        if (objetCasses == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].lampe = false;
        }
            
        else if (objetCasses == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].pioche = false;
        }
            
        else if (objetCasses == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].waggon = false;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        
        for (int i = 0; i < buttonsCasses.Count; i++)
        {
            buttonsCasses[i].SetActive(false);
        }
    }
    
    public void MalusJoueur2()
    {
        if (objetCasses == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].lampe = false;
        }
            
        else if (objetCasses == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].pioche = false;
        }
            
        else if (objetCasses == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].waggon = false;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsCasses.Count; i++)
        {
            buttonsCasses[i].SetActive(false);
        }
    }
    
    public void MalusJoueur3()
    {
        if (objetCasses == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].lampe = false;
        }
            
        else if (objetCasses == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].pioche = false;
        }
            
        else if (objetCasses == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].waggon = false;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsCasses.Count; i++)
        {
            buttonsCasses[i].SetActive(false);
        }
    }
    
    public void MalusJoueur4()
    {
        if (objetCasses == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].lampe = false;
        }
            
        else if (objetCasses == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].pioche = false;
        }
            
        else if (objetCasses == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].waggon = false;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsCasses.Count; i++)
        {
            buttonsCasses[i].SetActive(false);
        }
    }
    

    #endregion

    public void BonusPlay(string objet)
    {
        objetRepares = objet;
        
        foreach (Transform child in parentButtonsRepares.transform)
        {
            buttonsRepares.Add(child.gameObject);
        }
        
        for (int i = 0; i < buttonsRepares.Count; i++)
        {
            buttonsRepares[i].SetActive(true);
        }
    }

    #region BonusForAllPlayers
    public void BonusJoueur1()
    {
        if (objetRepares == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].lampe = true;
        }
            
        else if (objetRepares == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].pioche = true;
        }
            
        else if (objetRepares == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[0].waggon = true;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        
        for (int i = 0; i < buttonsRepares.Count; i++)
        {
            buttonsRepares[i].SetActive(false);
        }
    }
    
    public void BonusJoueur2()
    {
        if (objetRepares == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].lampe = true;
        }
            
        else if (objetRepares == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].pioche = true;
        }
            
        else if (objetRepares == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[1].waggon = true;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsRepares.Count; i++)
        {
            buttonsRepares[i].SetActive(false);
        }
    }
    
    public void BonusJoueur3()
    {
        if (objetRepares == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].lampe = true;
        }
            
        else if (objetRepares == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].pioche = true;
        }
            
        else if (objetRepares == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[2].waggon = true;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsRepares.Count; i++)
        {
            buttonsRepares[i].SetActive(false);
        }
    }
    
    public void BonusJoueur4()
    {
        if (objetRepares == "lampe")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].lampe = true;
        }
            
        else if (objetRepares == "pioche")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].pioche = true;
        }
            
        else if (objetRepares == "waggon")
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().players[3].waggon = true;
        }
            
        gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        
        for (int i = 0; i < buttonsRepares.Count; i++)
        {
            buttonsRepares[i].SetActive(false);
        }
    }
    

    #endregion
    
}
