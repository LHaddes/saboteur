﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CarteObjetsDetruits : ScriptableObject
{
    public string objet;
    
    public Gears gears;
    public TurnManager turnManager;

    public GameObject parentButtons;
    public List<GameObject> buttons;

    public void OnClick()
    {
        gears = FindObjectOfType<Gears>();
        turnManager = FindObjectOfType<TurnManager>();


        Debug.Log("onclic");
        
        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played)
        {
            FindObjectOfType<PlayableManager>().MalusPlay(objet);
        }


        if (gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing)
        {
            FindObjectOfType<DestroyCard>().Destroy();
            
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing = false;
        }
    }
}
