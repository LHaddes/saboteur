﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CarteChemin : ScriptableObject
{
    public Trail myTrail;
    
    public Sprite mySprite;

    public GameObject gears;

    public bool follow;
    
    void Start()
    {
        //gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    public void OnClick()
    {
        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played && 
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.canPlay)
        {
            follow = true;
            
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().
                PlayATrailCrad(myTrail);

            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
            FindObjectOfType<DestroyCard>().Destroy();
        }
        
        if (gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing)
        {
            FindObjectOfType<DestroyCard>().Destroy();
            
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing = false;
        }
    }
}
