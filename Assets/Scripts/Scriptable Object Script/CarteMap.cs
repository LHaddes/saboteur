﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CarteMap : ScriptableObject
{
    public new string name = "Map";
    
    public Gears gears;
    public TurnManager turnManager;
    
    public void OnClick()
    {
        gears = FindObjectOfType<Gears>();
        turnManager = FindObjectOfType<TurnManager>();

        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played)
        {
            turnManager.SetActionState(new ShowTileState(turnManager));
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
        }
        
        if (gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing)
        {
            FindObjectOfType<DestroyCard>().Destroy();
            
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing = false;
        }
    }
}
