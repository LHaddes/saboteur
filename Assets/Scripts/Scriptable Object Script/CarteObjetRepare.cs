﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CarteObjetRepare : ScriptableObject
{
    public string objet;
    
    public Gears gears;
    public TurnManager turnManager;

    public void OnClick()
    {
        gears = FindObjectOfType<Gears>();
        turnManager = FindObjectOfType<TurnManager>();
        
        //joeurChoisi.canPlay = true;
        Debug.Log(objet + " répare");
        
        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played)
        {
            FindObjectOfType<PlayableManager>().BonusPlay(objet);
        }
        
        if (gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing)
        {
            FindObjectOfType<DestroyCard>().Destroy();
            
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played = true;
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing = false;
        }
    }
}
