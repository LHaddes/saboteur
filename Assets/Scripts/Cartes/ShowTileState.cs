﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class ShowTileState : State
{
    public GameObject gears;

    public Vector2 mouseOnTheWorld;

    public Tilemap test; //tile map principale

    public Trail currentTrail;
    public Trail[] cardNextTo = new Trail[4];
    public bool[] connectedWithOne = new bool[4];

    public Text feedbackText = GameObject.Find("Feedback").GetComponent<Text>();
    public float cooldown = 0;
    public bool launchCooldown;
    

    public ShowTileState(TurnManager turnManager) : base(turnManager)
    {
    }

    public override void OnStateEnter()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        test = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().test;
    }

    public override void Tick()
    {
        mouseOnTheWorld = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(Input.mousePosition);
        var tileManager = GameObject.FindObjectOfType<TileManager>();

        if (Input.GetButtonDown("Fire1") && CanWatchCard())
        {
            for (int i = 0; i < tileManager.mines.Length; i++)
            {
                Debug.Log(tileManager.mines[i].trailTile);
                if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) == tileManager.mineTile)
                {
                    Debug.Log(tileManager.mines[i].gold);

                    
                    
                    if (tileManager.mines[i].gold > 0)
                    {
                        
                        feedbackText.text = "Le trésor est ici !";
                        launchCooldown = true;
                    }
                    else
                    {
                        feedbackText.text = "Le trésor n'est pas ici...";
                        launchCooldown = true;
                    }
                }
                else
                {
                    Debug.Log("no mine");
                }
            }


            
        }

        if (launchCooldown)
        {
            cooldown += Time.deltaTime;
            if (cooldown >= 1)
            {
                feedbackText.text = "";
                cooldown = 0;
                launchCooldown = false;
                
                turnManager.SetActionState(null);
            }
        }
    }

    public bool CanWatchCard()
    {
        if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) != null) //si il y a déja une carte sous la souris
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
