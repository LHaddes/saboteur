﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour
{
    public ScriptableObject carte;
    public Text cardType;
    
    void Start()
    {
        cardType.text = carte.name;
    }
}
