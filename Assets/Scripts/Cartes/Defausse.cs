﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defausse : MonoBehaviour
{
    private bool _canThrow;
    public Gears gears;

    void Awake()
    {
        gears = FindObjectOfType<Gears>();
    }

    public void Jet()
    {
        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played)
        {
            gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.isDefaussing = true;
        }
        
    }
}
