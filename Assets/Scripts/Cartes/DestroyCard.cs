﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCard : MonoBehaviour
{
    public void Destroy()
    {
        Destroy(this.gameObject);
    }
}
