﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCardManager : MonoBehaviour
{
    public List<GameObject> slotCartes;
    public Gears gears;

    void Awake()
    {
        gears = FindObjectOfType<Gears>();
    }
    
    public void DeleteChild()
    {
        if (!gears.GetComponent<Gears>().stateMachine.GetComponent<TurnManager>().currentPlayer.played)
        {
            Destroy(transform.GetChild(0).transform.gameObject);
        }
    }
}
