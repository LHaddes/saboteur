﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pioche : MonoBehaviour
{
    public List<GameObject> pioche;
    
    public List<GameObject> slotCartes;
    public int randomNumber;

    public bool change;
    public int tour = 0;

    public Player player;

    

    public void SlotRemove()
    {
        if (slotCartes.Count > 0)
        {
            for (int i = 0; i < 6; i++)
            {
                slotCartes.RemoveAt(0);
            }
        }
    }

    public void SlotUpdate()
    {
        for (int j = 0; j < player.main.transform.childCount; j++)
        {
            slotCartes.Add(player.main.transform.GetChild(j).gameObject);
        }
    }

    public void PlayerPioche()
    {
        for (int i = 0; i < slotCartes.Count; i++)
        {
            randomNumber = Random.Range(0, pioche.Count);

            if (slotCartes[i].transform.childCount == 0)
            {
                Instantiate(pioche[randomNumber], slotCartes[i].transform.transform.position, slotCartes[i].transform.rotation, slotCartes[i].transform);
                pioche.RemoveAt(randomNumber);
            }
        }
    }
}
