﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DeleteTileState : State
{
    public GameObject gears;

    public Vector2 mouseOnTheWorld;

    public Tilemap test; //tile map principale

    public Trail currentTrail;
    public Trail[] cardNextTo = new Trail[4];
    public bool[] connectedWithOne = new bool[4];

    public DeleteTileState(TurnManager turnManager) : base(turnManager)
    {
    }

    public override void OnStateEnter()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        test = gears.GetComponent<Gears>().tileManager.GetComponent<TileManager>().test;
    }

    public override void Tick()
    {
        mouseOnTheWorld = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetButtonDown("Fire1") && CanDeleteCard())
        {
            test.SetTile(test.WorldToCell(mouseOnTheWorld), null);
            turnManager.SetActionState(null);
        }
    }

    public bool CanDeleteCard()
    {
        if (test.GetTile(test.WorldToCell(mouseOnTheWorld)) != null) //si il y a déja une carte sous la souris
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
        
    

